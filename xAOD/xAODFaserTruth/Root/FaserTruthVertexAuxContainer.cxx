/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: FaserTruthVertexAuxContainer.cxx 624338 2014-10-27 15:08:55Z krasznaa $

// Local include(s):
#include "xAODFaserTruth/FaserTruthVertexAuxContainer.h"

namespace xAOD {

   FaserTruthVertexAuxContainer::FaserTruthVertexAuxContainer()
   : AuxContainerBase() {

      AUX_VARIABLE( id );
      AUX_VARIABLE( barcode );
      AUX_VARIABLE( incomingParticleLinks );
      AUX_VARIABLE( outgoingParticleLinks );
      AUX_VARIABLE( x );
      AUX_VARIABLE( y );
      AUX_VARIABLE( z );
      AUX_VARIABLE( t );
   }

} // namespace xAOD
