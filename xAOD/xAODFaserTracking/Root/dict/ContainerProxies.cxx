// EDM include(s):
#include "xAODCore/AddDVProxy.h"

// Local include(s):
#include "xAODFaserTracking/TrackContainer.h"
#include "xAODFaserTracking/StripClusterContainer.h"
#include "xAODFaserTracking/StripRawDataContainer.h"

// Set up the collection proxies:
ADD_NS_DV_PROXY( xAOD, TrackContainer );
ADD_NS_DV_PROXY( xAOD, StripClusterContainer );
ADD_NS_DV_PROXY( xAOD, StripRawDataContainer );