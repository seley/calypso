################################################################################
# Package: xAODFaserTrackingAthenaPool
################################################################################

# Declare the package name:
atlas_subdir( xAODFaserTrackingAthenaPool )

# External dependencies:
find_package( ROOT COMPONENTS Core )

# Component(s) in the package:
atlas_add_poolcnv_library( xAODFaserTrackingAthenaPoolPoolCnv
   src/*.h src/*.cxx
   FILES xAODFaserTracking/StripClusterContainer.h
   xAODFaserTracking/StripClusterAuxContainer.h
   TYPES_WITH_NAMESPACE xAOD::StripClusterContainer xAOD::StripClusterAuxContainer
   CNV_PFX xAOD
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} AthContainers AthenaKernel
   AthenaPoolCnvSvcLib AthenaPoolUtilities xAODFaserTracking xAODTrackingCnvLib   )